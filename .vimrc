set shell=/bin/bash
set term=xterm-256color
filetype on
syntax on
filetype plugin on
"set ofu=syntaxcomplete#Complete
set ruler
set cursorline
set number
colorscheme desert
au BufNewFile,BufRead *.less set filetype=less
set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=4
set scrolloff=2
set ai
set wildmenu
set backspace=eol,indent,start
au BufRead,BufNewFile *.jsi	set filetype=javascript
set ffs=unix,dos,mac
set fenc=utf-8
inoremap kj <ESC>
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-h> <c-w>h
map <c-l> <c-w>l
let mapleader=","

